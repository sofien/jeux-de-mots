# Jeux-De-Mots
#### Description:<br/>
Jeux-De-Mots est un dictionnaire d'associations lexicales.<br />
Vous y trouverez plusieurs mots avec leur definitions tel un dictionnaire mais vous trouverez encore plus d'informations.

----
#### Comment l'utiliser:
Entrez un mot dans la barre de recherche ou cliquez sur un des mots present dans la page principale.<br />
Une foi le mot choisi, vous serez redirigé vers la page contenant la/les definition(s) du mot choisi.