-- --------------------------
-- Auteur: Sofien Benharchache
-- date: jeudi 29 septembre 2016
-- Cours: e-application
-- --------------------------
-- fichier: bd_jdm.sql
-- description:
-- --- fichier contenant toutes les tables pour la base de donnée du pojet diko du jeu de mots
-- --------------------------

system echo '--------------------\n-- Mise à jour\n--------------------\n' ;
SELECT CURRENT_DATE(), CURRENT_TIME();

-- --------
-- Mise en place de l'encodage:
-- --------
SET NAMES 'utf8';
SET CHARACTER SET utf8;

-- ----------
-- Suppression des tables pour leur mises a jour
-- ----------
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE Node;
DROP TABLE Relations;
DROP TABLE Relation_type;
-- DROP TABLE Relation_node_with_note;
-- DROP TABLE Relation_node_with_relation_type;
SET FOREIGN_KEY_CHECKS=1;

-- ----------
-- Toutes les tables
-- ----------
CREATE TABLE Node(
  eid             INTEGER PRIMARY KEY NOT NULL,
  name            TEXT NOT NULL,
  t               INTEGER NOT NULL,
  w               INTEGER NOT NULL,
  formated_name   TEXT
) engine=InnoDB;

CREATE TABLE Relation_type(
  rtid         INTEGER PRIMARY KEY NOT NULL,
  name         VARCHAR(300) NOT NULL,
  extends_name TEXT,
  info         TEXT
) engine=InnoDB;

CREATE TABLE Relations(
  bdid  INTEGER PRIMARY KEY NOT NULL,
  eid1  INTEGER NOT NULL,
  eid2  INTEGER NOT NULL,
  t     INTEGER NOT NULL,
  w     INTEGER NOT NULL
  -- CONSTRAINT fk_eid_1 FOREIGN KEY (eid1) REFERENCES Node(eid),
  -- CONSTRAINT fk_eid_2 FOREIGN KEY (eid2) REFERENCES Node(eid)
  -- -- CONSTRAINT fk_eid_2 FOREIGN KEY (eid2) REFERENCES Relation_type(rtid)
) engine=InnoDB;


-- On ajoute un index pour booster la recherche
ALTER TABLE Relations ADD INDEX index_eid1_eid2 (eid1, eid2);
ALTER TABLE Node ADD INDEX index_eid (eid);
ALTER TABLE Relation_type ADD INDEX index_rtid (rtid);


-- La ligne de commande suivante va nous permetre de lister les infos sur le cache de mysql: 
    -- SHOW VARIABLES LIKE 'query_cache_%';
