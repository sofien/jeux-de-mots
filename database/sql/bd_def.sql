-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Jeu 29 Septembre 2016 à 11:03
-- Version du serveur: 5.5.52-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `Lafourcade_RezoLexical`
--

-- --------------------------------------------------------

--
-- Structure de la table `TermDef`
--

CREATE TABLE IF NOT EXISTS `TermDef` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `termid` int(11) NOT NULL,
  `def` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `playerid` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `termid` (`termid`),
  KEY `playerid` (`playerid`),
  FULLTEXT KEY `ft` (`def`)
) ENGINE=MyISAM  AUTO_INCREMENT=685146 ;
