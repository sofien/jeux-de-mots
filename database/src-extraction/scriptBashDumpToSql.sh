#!/bin/bash
#pour debuger il faut taper : $ bash -x monfichier.sh

premierN=true
premierR=true
premierRT=true

################################################################################################
############    ----------    RelationType
function dumpRelationTypeTosql(){
    ######  DEBUT DU PARCOURS SUR CHAQUE LIGNE
    if [[ $1 =~ ^rtid=([0-9]+)\|name=(\".*\")\|nom_etendu=(\".*\")\|info=(\".*\")$ ]];
    then
      #Si on a une colone vide ("") alors on met NULL sinon on affiche l'info
      if [[ ${#BASH_REMATCH[4]} -gt 2 ]];
      then
        echo "(${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},${BASH_REMATCH[4]})"
      else
        echo "(${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},NULL)"
      fi
    fi
  ######  FIN DU PARCOURS SUR CHAQUE LIGNE
}

function parseRelationType(){
	res=$(dumpRelationTypeTosql $1)
	if [[ $res != "" ]]; then
	 	if [[ $premierRT = true ]]; then
        	premierRT=false
        else
        	echo ',' >> relationTypes.sql
      	fi
		echo -n $res >> relationTypes.sql
	fi
}


################################################################################################
############    ----------    Relation
function dumpRelationTosql(){
    ######  DEBUT DU PARCOURS SUR CHAQUE LIGNE
    if [[ $ligne =~ ^rid=([0-9]+)\|n1=([0-9]+)\|n2=([0-9]+)\|t=([0-9]+)\|w=([0-9]+)$ ]];
    then
      echo "(${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},${BASH_REMATCH[4]},${BASH_REMATCH[5]})"
    fi
  ######  FIN DU PARCOURS SUR CHAQUE LIGNE
}

function parseRelation(){
	res=$(dumpRelationTosql $1)
	if [[ $res != "" ]]; then
		if [[ $premierR = true ]]; then
        	premierR=false
        else
        	echo ',' >> relations.sql
      	fi
		echo -n $res >> relations.sql
	fi
}

################################################################################################
############    ----------    Node
function dumpNodeTosql(){
    ######  DEBUT DU PARCOURS SUR CHAQUE LIGNE
    if [[ $ligne =~ ^eid=([0-9]+)\|n=(\".*\")\|t=([0-9]+)\|w=([0-9]+)(\|nf=)?(\".*\")?$ ]];
    then
      if [ -z "${BASH_REMATCH[6]}" ];
      then
        echo "(${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},${BASH_REMATCH[4]},NULL)" 
      else
        echo "(${BASH_REMATCH[1]},${BASH_REMATCH[2]},${BASH_REMATCH[3]},${BASH_REMATCH[4]},${BASH_REMATCH[6]})" 
      fi
    fi
  ######  FIN DU PARCOURS SUR CHAQUE LIGNE
}

function parseNode(){
	res=$(dumpNodeTosql $1)
	if [[ $res != "" ]]; then
		if [[ $premierN = true ]]; then
        	premierN=false
        else
        	echo ',' >> nodes.sql
      	fi
		echo -n $res >> nodes.sql
	fi
}




#######################
### decouperDumpToSql
#######################
decouperDumpToSql() {
  echo "Debut decouperDumpToSql"
  ##Variable
  debut_n=false
  debut_r=false
  debut_rt=false
  ##Creation des fichiers
  echo "INSERT INTO Node (eid, name, t, w, formated_name) VALUES" > nodes.sql
  echo "INSERT INTO Relations (bdid, eid1, eid2, t, w) VALUES" >  relations.sql
  echo "INSERT INTO Relation_type (rtid, name, extends_name, info) VALUES" > relationTypes.sql
  ##Parcours des fichiers
  cat "../dump/JDM-LEXICALNET-FR/01012017-LEXICALNET-JEUXDEMOTS-FR-NOHTML.txt" | while  read ligne ; do
    if [[ $ligne = "// ---- RELATION TYPES" ]]; then
      debut_n=false
      debut_r=false
      debut_rt=true
      echo $ligne
    elif [[ $ligne = "// -- NODES" ]]; then
      debut_n=true
      debut_r=false
      debut_rt=false
	elif [[ $ligne = "// -- RELATIONS" ]]; then
      debut_n=false
      debut_r=true
      debut_rt=false
    elif [[ $debut_n = true ]]; then
  		parseNode $ligne
  	elif [[ $debut_r = true ]]; then
  		parseRelation $ligne
  	elif [[ $debut_rt = true ]]; then
  		parseRelationType $ligne
    fi
  done
  ##On termine avec un point virgule a la fin de chaque fichier
  echo ";" >> nodes.sql
  echo ";" >> relations.sql
  echo ";" >> relationTypes.sql
  echo "Fin decouperDumpToSql"
}

decouperDumpToSql
