<?php

namespace JDM\DictionnaireBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Response;
class PagesController extends Controller
{
    /*public function indexAction()
    {
        return $this->render('JDMDictionnaireBundle::index.html.twig');
    }*/
    public function researchAction(Request $request)
    {
        $infos1=array();
        $infos1[]='femme';
        $infos1[]='homme';
        $infos1[]='enfant';
        $infos1[]='animal';
        $infos1[]='maison';
        $infos1[]='voiture';
        $infos1[]='eau';
        $infos1[]='personne';
        $infos1[]='cuisine';
        $infos1[]='manger';
        $infos1[]='musique';
        $infos1[]='mer';
        $infos1[]='ville';
        $infos1[]='maladie';
        $infos1[]='&eacute;cole';
        $infos1[]='film';
        $infos1[]='oiseau';
        $infos1[]='***';
        $infos1[]='argent';
        $infos1[]='livre';
        $infos1[]='rue';
        $infos1[]='amour';
        $infos1[]='lettre';
        $infos1[]='cin&eacute;ma';
        $infos1[]='main';
        $infos1[]='poisson';
        $infos1[]='m&eacute;decine';
        $infos1[]='mort';
        $infos1[]='fruit';
        $infos1[]='individu';
        $infos1[]='dormir';
        $infos1[]='fille';
        $infos1[]='lit';
        $infos1[]='bateau';
        $infos1[]='t&ecirc;te';
        $infos1[]='guerre';
        $infos1[]='avion';
        $infos1[]='chien';
        $infos1[]='m&eacute;decin';
        $infos1[]='arbre';
        $infos1[]='chat';
        $infos1[]='sexe';
        $infos1[]='rouge';
        $infos1[]='chambre';
        $infos1[]='jeu';
        $infos1[]='soleil';
        $infos1[]='restaurant';
        $infos1[]='pays';
        $infos1[]='peur';




        $infos2=array();
        $infos2[]='Bloc 2 - Premiere ligne 1';
        $infos2[]='Bloc 2 - Premiere ligne 2';
        $infos2[]='Bloc 2 - Premiere ligne 3';

        $node_rand = array();
        for ($i=0; $i < 50; $i++) {
            $node_rand[] = rand (0 , 99999);
        }

        $query_rand = $this  ->getDoctrine()
                        ->getManager()
                        ->createQuery( 'SELECT n.name
                                        FROM  JDMDictionnaireBundle:Node n
                                        WHERE n.eid=:idword0
                                        OR n.eid=:idword1
                                        OR n.eid=:idword2
                                        OR n.eid=:idword3
                                        OR n.eid=:idword4
                                        OR n.eid=:idword5
                                        OR n.eid=:idword6
                                        OR n.eid=:idword7
                                        OR n.eid=:idword8
                                        OR n.eid=:idword9
                                        OR n.eid=:idword10
                                        OR n.eid=:idword11
                                        OR n.eid=:idword12
                                        OR n.eid=:idword13
                                        OR n.eid=:idword14
                                        OR n.eid=:idword15
                                        OR n.eid=:idword16
                                        OR n.eid=:idword17
                                        OR n.eid=:idword18
                                        OR n.eid=:idword19
                                        OR n.eid=:idword20
                                        OR n.eid=:idword21
                                        OR n.eid=:idword22
                                        OR n.eid=:idword23
                                        OR n.eid=:idword24
                                        OR n.eid=:idword25
                                        OR n.eid=:idword26
                                        OR n.eid=:idword27
                                        OR n.eid=:idword28
                                        OR n.eid=:idword29
                                        OR n.eid=:idword30
                                        OR n.eid=:idword31
                                        OR n.eid=:idword32
                                        OR n.eid=:idword33
                                        OR n.eid=:idword34
                                        OR n.eid=:idword35
                                        OR n.eid=:idword36
                                        OR n.eid=:idword37
                                        OR n.eid=:idword38
                                        OR n.eid=:idword39
                                        OR n.eid=:idword40
                                        OR n.eid=:idword41
                                        OR n.eid=:idword42
                                        OR n.eid=:idword43
                                        OR n.eid=:idword44
                                        OR n.eid=:idword45
                                        OR n.eid=:idword46
                                        OR n.eid=:idword47
                                        OR n.eid=:idword48
                                        OR n.eid=:idword49'
                                    )
                                    ->setParameter('idword0', $node_rand[0])
                                    ->setParameter('idword1', $node_rand[1])
                                    ->setParameter('idword2', $node_rand[2])
                                    ->setParameter('idword3', $node_rand[3])
                                    ->setParameter('idword4', $node_rand[4])
                                    ->setParameter('idword5', $node_rand[5])
                                    ->setParameter('idword6', $node_rand[6])
                                    ->setParameter('idword7', $node_rand[7])
                                    ->setParameter('idword8', $node_rand[8])
                                    ->setParameter('idword9', $node_rand[9])
                                    ->setParameter('idword10', $node_rand[10])
                                    ->setParameter('idword11', $node_rand[11])
                                    ->setParameter('idword12', $node_rand[12])
                                    ->setParameter('idword13', $node_rand[13])
                                    ->setParameter('idword14', $node_rand[14])
                                    ->setParameter('idword15', $node_rand[15])
                                    ->setParameter('idword16', $node_rand[16])
                                    ->setParameter('idword17', $node_rand[17])
                                    ->setParameter('idword18', $node_rand[18])
                                    ->setParameter('idword19', $node_rand[19])
                                    ->setParameter('idword20', $node_rand[20])
                                    ->setParameter('idword21', $node_rand[21])
                                    ->setParameter('idword22', $node_rand[22])
                                    ->setParameter('idword23', $node_rand[23])
                                    ->setParameter('idword24', $node_rand[24])
                                    ->setParameter('idword25', $node_rand[25])
                                    ->setParameter('idword26', $node_rand[26])
                                    ->setParameter('idword27', $node_rand[27])
                                    ->setParameter('idword28', $node_rand[28])
                                    ->setParameter('idword29', $node_rand[29])
                                    ->setParameter('idword30', $node_rand[30])
                                    ->setParameter('idword31', $node_rand[31])
                                    ->setParameter('idword32', $node_rand[32])
                                    ->setParameter('idword33', $node_rand[33])
                                    ->setParameter('idword34', $node_rand[34])
                                    ->setParameter('idword35', $node_rand[35])
                                    ->setParameter('idword36', $node_rand[36])
                                    ->setParameter('idword37', $node_rand[37])
                                    ->setParameter('idword38', $node_rand[38])
                                    ->setParameter('idword39', $node_rand[39])
                                    ->setParameter('idword40', $node_rand[40])
                                    ->setParameter('idword41', $node_rand[41])
                                    ->setParameter('idword42', $node_rand[42])
                                    ->setParameter('idword43', $node_rand[43])
                                    ->setParameter('idword44', $node_rand[44])
                                    ->setParameter('idword45', $node_rand[45])
                                    ->setParameter('idword46', $node_rand[46])
                                    ->setParameter('idword47', $node_rand[47])
                                    ->setParameter('idword48', $node_rand[48])
                                    ->setParameter('idword49', $node_rand[49])
                                    ->setMaxResults(50)
                        ;
        $random_list = $query_rand->getResult();



        //On genere les fichier recherche
        $cmdline = 'bash res/recupInfos.sh';
        $last_line = system($cmdline);
        //die($last_line);
        //die(var_dump($last_line));

        //On attend la generation des fichier.
        $monfichier = fopen('processus.soka', 'r'); //On ouvre le fichier processus.nes
        $ligne = fgets($monfichier); //On lit la premiere ligne
        //if(strlen($ligne)==0) //Si le fichier est vide
        //    return $this->redirectToRoute('ign_info_git_index'); //Alors on redirige la route vers l'index (la barre de recherche)

        while(intval($ligne) <= 0){ // Tant que la valeur de la ligne est inferieur ou égal a zero.
            fseek($monfichier, 0); // On remet le curseur au debut du fichier
            $ligne = fgets($monfichier); //On lit la premiere ligne
        }
        fclose($monfichier); //On ferme le fichier processus.nes


        // 1 : on ouvre le fichier lci.list
        $fichierLCI = fopen('lci.list', 'r');
        $ligne      = fgets($fichierLCI);
        $ligne_lci_1 = fgets($fichierLCI);
        $ligne_lci_2 = fgets($fichierLCI);
        $ligne_lci_3 = fgets($fichierLCI);
        $ligne_lci_4 = fgets($fichierLCI);
        fclose($fichierLCI);

        // 1 : on ouvre le fichier macg.list
        $fichierMACG = fopen('macg.list', 'r');
        $ligne      = fgets($fichierMACG);
        $ligne_macg_1 = fgets($fichierMACG);
        $ligne_macg_2 = fgets($fichierMACG);
        $ligne_macg_3 = fgets($fichierMACG);
        $ligne_macg_4 = fgets($fichierMACG);
        fclose($fichierMACG);

        //die($ligne_4);
        //die(var_dump($ligne_lci_2));
        $lci_data = array();
        $lci_data[] = explode(" ", $ligne_lci_1);
        $lci_data[] = explode(" ", $ligne_lci_2);
        $lci_data[] = explode(" ", $ligne_lci_3);
        $lci_data[] = explode(" ", $ligne_lci_4);

        $macg_data = array();
        $macg_data[] = explode(" ", $ligne_macg_1);
        $macg_data[] = explode(" ", $ligne_macg_2);
        $macg_data[] = explode(" ", $ligne_macg_3);
        $macg_data[] = explode(" ", $ligne_macg_4);
        //die(var_dump($macg_data));
        if($ligne_macg_1 == "")
            $macg_data = $infos2;
        if($ligne_lci_1 == "")
            $lci_data = $infos2;

        $data = array();
        $form = $this->createFormBuilder($data)
                     ->add('word', TextType::class, array('attr' => array('class' => 'input-recherche form-control wordset', 'list'=> 'wordsetlist', 'placeholder'=>'Entrez un mot', 'style'=>'height:25px;', 'onclick' => 'getload()'), 'label'=>false))
                     ->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* On recupere les données */
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $wordset = $em->getRepository('JDMDictionnaireBundle:Node')->findBy(array('name'=> $data['word']));

            /* On change de page pour aller a la page principale */
            return $this->redirectToRoute('jdm_dictionnaire_dictionarypage', array('word' => $data['word'], ));
        }
        //die(var_dump($random_list));

        return $this->render('JDMDictionnaireBundle:Pages:research.html.twig', array('form' => $form->createView(), 'infos1' => $infos1, 'infos2' => $random_list, 'infos3' => $lci_data, 'infos4' => $macg_data));
    }

    public function dictionaryAction($word, $sortType, $page, $numPage, Request $request)
    {

        /*$cacheDriver = new \Doctrine\Common\Cache\XcacheCache();

        if ($cacheDriver->contains('cache_id')) {
            echo 'cache exists';
            $array = $cacheDriver->fetch('my_array');
            echo " \n ";
            echo $array;
            die();
        } else {
            echo 'cache does not exist';
            $cacheDriver->save('cache_id', 'my_data');
        }*/

           //$response = new Response();
           //$response->setMaxAge(300);
           // Check that the Response is not modified for the given Request
/*           if (!$response->isNotModified($request)) {
               $date = new \DateTime();
               $response->setLastModified($date);
               $response->setContent($this->renderView('JDMDictionnaireBundle:Pages:dictionary.html.twig', array('form' => $form->createView(), 'word'=>$word, 'definition' => $definition, 'relationtype' => $relationType, 'node' => $noderes_pagination, 'polarite' => $polarite)));
               }
*/
        //ini_set("memory_limit",-1);
        if($page === -1){
            $definition = array();
            $definition[]="Definition 1 pour le fun de dezd ed zdede zd ez dezd dezde gtrgt r dedez defre fr egtgtr  ez dezd dezde gtrgt r dedez defre fr egtgtr  ez dezd dezde gtrgt r dedez defre fr egtgtr g";
            $definition[]= array(1=>'Definition 2 pour le fun 1',2 => 'Definition 2 pour le fun 2');
            $definition[]="Definition 3 pour le fun";
            $definition[]= array(1 => 'Definition 4 pour le fun 1', 2 => 'Definition 4 pour le fun 2');


            $query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT  DISTINCT r.t, rt.extendsName, n2.name, n2.formatedName, n1.eid, r.w, rt.info
                                            FROM   JDMDictionnaireBundle:Relations r, JDMDictionnaireBundle:Relation_type rt, JDMDictionnaireBundle:Node n1, JDMDictionnaireBundle:Node n2
                                            WHERE  n1.name=:word
                                            AND r.t<>444
                                            AND r.t<>555
                                            AND r.t=rt.rtid
                                            AND r.eid1=n1.eid
                                            AND r.eid2=n2.eid
                                            AND n2.name<>:ignorer1
                                            AND n2.name<>:ignorer2
                                            AND n2.name<>:ignorer3
                                            AND n2.name<>:ignorer4
                                            AND n2.name<>:ignorer5
                                            AND n2.name<>:ignorer6
                                            AND n2.name<>:ignorer7
                                            AND n2.name<>:ignorer8
                                            AND n2.name<>:ignorer9
                                            AND n2.name<>:ignorer10
                                            AND n2.name<>:ignorer11
                                            AND n2.name<>:ignorer12
                                            AND n2.name<>:ignorer13
                                            AND n2.name<>:ignorer14
                                            AND n2.name<>:ignorer15
                                            AND n2.name<>:ignorer16
                                            AND n2.name<>:ignorer17
                                            AND n2.name<>:ignorer18
                                            AND n2.name<>:ignorer19
                                            AND n2.name<>:ignorer20
                                            AND n2.name<>:ignorer21
                                            AND n2.name<>:ignorer22
                                            AND n2.name<>:ignorer23
                                            AND n2.name<>:ignorer24
                                            AND n2.name<>:ignorer25
                                            AND n2.name<>:ignorer26
                                            AND n2.name<>:ignorer27
                                            AND n2.name<>:ignorer28
                                            AND n2.name<>:ignorer29
                                            AND n2.name<>:ignorer30
                                            AND n2.name<>:ignorer31
                                            AND n2.name<>:ignorer32
                                            AND n2.name<>:ignorer33
                                            '
                                            //GROUP BY r.t, rt.extendsName, n2.name, n2.formatedName, n1.eid, r.w
                                        )
                                        ->setParameter('ignorer1',  "_INFO-NO-MORE-QUESTION")
                                        ->setParameter('ignorer2',  "_INFO-POS-NP")
                                        ->setParameter('ignorer3',  "_INFO-POS-MASC")
                                        ->setParameter('ignorer4',  "_INFO-POS-PLUR")
                                        ->setParameter('ignorer5',  "_INFO-POS-VERB")
                                        ->setParameter('ignorer6',  "_INFO-POS-NC")
                                        ->setParameter('ignorer7',  "_INFO-POS-FEM")
                                        ->setParameter('ignorer8',  "_INFO-POS-SING")
                                        ->setParameter('ignorer9',  "_INFO-POS-VER")
                                        ->setParameter('ignorer10',  "_INFO-LPOP")
                                        ->setParameter('ignorer11',  "_INFO-POS-ADJ")
                                        ->setParameter('ignorer12',  "_INFO-LSOUT")
                                        ->setParameter('ignorer13',  "_INFO-POS-ADV-EXPR")
                                        ->setParameter('ignorer14',  "_INFO-POS-ADV")
                                        ->setParameter('ignorer15',  "_TERM_GROUP")
                                        ->setParameter('ignorer16',  "_SEX_NEUTRAL")
                                        ->setParameter('ignorer17',  "_POL-POS_PC")
                                        ->setParameter('ignorer18',  "_POL-NEUTRE_PC")
                                        ->setParameter('ignorer19',  "_POL-NEG_PC")
                                        ->setParameter('ignorer20',  "_INFO-WIKI-NO")
                                        ->setParameter('ignorer21',  "_INFO-WIKI-YES")
                                        ->setParameter('ignorer22',  "_INFO-POS")
                                        ->setParameter('ignorer23',  "_POLIT_6FN")
                                        ->setParameter('ignorer24',  "_POLIT_1ECO")
                                        ->setParameter('ignorer25',  "_POLIT_2PG")
                                        ->setParameter('ignorer26',  "_POLIT_5UMP")
                                        ->setParameter('ignorer27',  "_POLIT_3PS")
                                        ->setParameter('ignorer28',  "_POLIT_4MD")
                                        ->setParameter('ignorer29',  "_POLIT_7NOPOL")
                                        ->setParameter('ignorer30',  "_INFO")
                                        ->setParameter('ignorer31',  "_OCC-IGNORE")
                                        ->setParameter('ignorer32',  "_INFO-CNRTL-YES")
                                        ->setParameter('ignorer33',  "_INFO-CNRTL-NO")

                            /*->createQuery( 'SELECT  r.t, rt.extendsName, n2.name, n2.formatedName
                                            FROM   JDMDictionnaireBundle:Relations r, JDMDictionnaireBundle:Relation_type rt, JDMDictionnaireBundle:Node n1, JDMDictionnaireBundle:Node n2
                                            WHERE  n1.name=:word
                                            AND r.t=rt.rtid
                                            AND r.t<>4
                                            AND r.t<>12
                                            AND r.t<>18
                                            AND r.t<>19
                                            AND r.t<>23
                                            AND r.t<>24
                                            AND r.t<>26
                                            AND r.t<>27
                                            AND r.t<>42
                                            AND r.t<>48
                                            AND r.t<>47
                                            AND r.t<>46
                                            AND r.t<>45
                                            AND r.t<>66
                                            AND r.t<>114
                                            AND r.t<>118
                                            AND r.t<>155
                                            AND r.t<>156
                                            AND r.t<>444
                                            AND r.t<>555
                                            AND r.t<>666
                                            AND r.t<>777
                                            AND r.t<>998
                                            AND r.t<>999
                                            AND r.t<>1000
                                            AND r.t<>1001
                                            AND r.t<>1002
                                            AND r.t<>2001
                                            AND r.eid1=n1.eid
                                            AND r.eid2=n2.eid
                                            GROUP BY r.t, rt.extendsName, n2.name, n2.formatedName'
                                        )*/
                            ->setParameter('word', $word);
            //$query->useQueryCache(true);
            //$query->useResultCache(true);
            //$query->setResultCacheLifetime(3600);
            //$node = $query->getOneOrNullResult();
            $node = $query->getResult();

            /*-----------------------------------------------------------*/
            /* Verification pour savoir si le mot existe ou nom */
            if (null === $node || count($node)===0) { //Verification pour savoir si le mot existe ou nom
              throw new NotFoundHttpException("Le mot ".$word." n'existe pas.");
            }


            /*-----------------------------------------------------------*/
            /* Requete pour la polarite */
            $idword=$node[0]["eid"];
            $query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT r.w, r.eid2
                                            FROM  JDMDictionnaireBundle:Relations r
                                            WHERE r.eid1=:idword
                                            AND (r.eid2=254878
                                            OR r.eid2=254877
                                            OR r.eid2=254876)'
                                        )
                            ->setParameter('idword', $idword);
            $polarite = $query->getResult();
            //die(var_dump($polarite_neg));

            /*-----------------------------------------------------------*/
            /* regroupement des type de relation dans un tableau */
            $relationType = array();
            for($n =0 ; $n< count($node); $n++){
                if(!in_array($node[$n]["extendsName"], $relationType))
                    $relationType[]=$node[$n]["extendsName"];
            }

            /*-----------------------------------------------------------*/
            /* regroupement des valeur par les types de relation */
            $nodeRes = array();
            for($n =0 ; $n< count($node); $n++){
                for($rt =0 ; $rt< count($relationType); $rt++){
                    if($node[$n]['extendsName']===$relationType[$rt]){
                            if($relationType[$rt] === "information potentielle"){
                                $node[$n]['name']=getPOT($node, $n);
                            }
                            else if($relationType[$rt] === "r_pos"){
                                $node[$n]['name']=getPOS($node, $n);
                            }
                            $nodeRes[$relationType[$rt]][]=$node[$n];
                            continue;
                        //}
                    }
                }
            }

            /*for($rt =0 ; $rt< count($relationType)-10; $rt++){
                $nodeRes[$relationType[$rt]]=executer_tri_rapide($nodeRes[$relationType[$rt]], 0, count($nodeRes[$relationType[$rt]]));
            }*/

            /*-----------------------------------------------------------*/
            /* requetes de Definition */
            /*requete pour la definition*/
            $query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT d.def
                                            FROM  JDMDictionnaireBundle:Definition d
                                            WHERE d.termid=:idword'
                                        )
                            ->setParameter('idword', $idword);
            $definition = $query->getResult();
            //die(var_dump($definition));

            $def = array();
            for ($i=0; $i < count($definition); $i++) {
                $def[] = $this->extractDef($definition[$i]['def']);
                $definition[$i]['def'] = $def;
            }

            $definition=$def;
            //die(var_dump($definition));



        }
        /*-----------------------------------------------------------*/
        /* pagination */
        /*for ($i=0; $i < count($relationType); $i++) {
            $noderes_pagination[$relationType[$i]] = $this->get('knp_paginator')->paginate($nodeRes[$relationType[$i]], $request->query->get('page'.$i, 1), 50);
        }*/
        //$paginator  = $this->get('knp_paginator')->paginate($query, $request->query->get('page', 1), 10);
        $noderes_pagination=$nodeRes;

        /*-----------------------------------------------------------*/
        /* Formulaire de recherche */
        $data = array();
        $form = $this->createFormBuilder($data)
                     ->add('word', TextType::class, array('attr' => array(/* 'id' => 'research-word_', */
                                                                           'class' => 'input-recherche-barre form-control wordset', 
                                                                           'list'=> 'wordsetlist', 
                                                                           'placeholder'=>'Entrez un mot', 
                                                                           'label'=>false), 'label'=>false))
                     ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /* On recupere les données */
            $data = $form->getData();
            /* On change de page pour aller a la page principale */
            return $this->redirectToRoute('jdm_dictionnaire_dictionarypage', array('word' => $data['word']));
        }





        //die(var_dump($node));
        return $this->render('JDMDictionnaireBundle:Pages:dictionary.html.twig', array('form' => $form->createView(), 'word'=>$word, 'definition' => $definition, 'relationtype' => $relationType, 'node' => $noderes_pagination, 'polarite' => $polarite));
    }
    function multiexplode ($delimiters,$string) {
        $del = $delimiters[0];
        $ready = str_replace($delimiters, $del, $string);
        $launch = explode($del, $ready);
        $launch_tmp=array();
        for ($i=0; $i < count($launch); $i++) {
            if(strcmp($launch[$i], "") !== 0
            && strcmp($launch[$i], " ") !== 0
            && strcmp($launch[$i], "  ") !== 0
            && $launch[$i] !== NULL
            ){
                $launch_tmp[] = $launch[$i];
                //echo "Val de launch = ";
                //echo $launch[$i];
                //echo "<hr/>";
                //if($launch[$i] === "")
                //    echo "__eql__";
            }
        }
        //if($launch[9] == "")
        //    echo "vrai L  ";
        //    else echo "faut L   ";
        //die(var_dump($launch_tmp[9]));
        return  $launch_tmp;
    }

    public function extractDef($definition){
        $defTmp = array();
        $defTmp = $this->multiexplode(array("<br />","<br>","<br/>", "</ i>","</i>","<i>"),$definition);
        $defTitre = $defTmp[0];
        $defRest = array();
        for ($i=1; $i < count($defTmp); $i++) {
            $defRest[] = $defTmp[$i];//$this->multiexplode(array(),$defTm[$i]);
        }
        $defTmp = array();
        $defTmp[] = $defTitre;
        $defTmp[] = $defRest;
        //die(var_dump($defTmp));

        return $defTmp;
    }

    public function autocompletionAction($word, Request $request){
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->add('select', 'n.name')
            ->add('from', 'JDMDictionnaireBundle:Node n')
            ->add('where', $qb->expr()->like('n.name', '?1'))
            ->setMaxResults('7')
            ->setParameter(1, $word.'%');
        $query = $qb->getQuery();
        $ndset = $query->getResult();
        $nodeset=NULL;
        foreach ($ndset as $key => $value) {
            $nodeset[]=$value;
        }
        //$nodeset="soka";
        //die(var_dump($nodeset));

        $response = new JsonResponse();
        return $response->setData(array('nodeset'=>$nodeset));
    }



    public function sort_relation_node($node, $val){
        if(count($node)==0){
            $node[]=$val;
            return ;
        }
        $node_tmp=array();
        for ($i=0; $i < count($node); $i++) {
            if($node[$i]['w']<$val['w'])
                $node_tmp[] = $node[$i];
            else{
                $node_tmp[] = $val;
                $node_tmp[] = $node[$i];
            }
        }
        return $node_tmp;
    }
}


function executer_tri_rapide (&$t, $debut, $fin) {
    if ($debut < $fin) {
        $indice_pivot = partitionner($t, $debut, $fin);
        executer_tri_rapide($t, $debut, $indice_pivot - 1);
        executer_tri_rapide($t, $indice_pivot + 1, $fin);
    }
}

function partitionner (&$t, $debut, $fin) {
    $valeur_pivot = $t[$debut]['w'];
    $d = $debut + 1;
    $f = $fin;
    while ($d < $f) {
        while ($d < $f && $t[$f]['w'] >= $valeur_pivot) $f--;
        while ($d < $f && $t[$d]['w'] <= $valeur_pivot) $d++;
        $temp = $t[$d];
        $t[$d] = $t[$f];
        $t[$f] = $temp;
    }
    if ($t[$d]['w'] > $valeur_pivot) $d--;
    $t[$debut] = $t[$d];
    $t[$d] = $valeur_pivot;
    return $d;
}

function getPOT(&$node, &$n){
    $node_val="";
    if($node[$n]['name'] == "_INFO-SEM-PERS")
        $node_val="une personne";
    else if($node[$n]['name'] == "_INFO-SEM-ORGA")
        $node_val="une organisation";
    else if($node[$n]['name'] == "_INFO-SEM-PLACE")
        $node_val="un lieu";
    else if($node[$n]['name'] == "_INFO-SEM-EVENT")
        $node_val="un evenement";
    else if($node[$n]['name'] == "_INFO-SEM-THING-CONCRETE")
        $node_val="une chose concrète";
    else if($node[$n]['name'] == "_INFO-SEM-SUBST")
        $node_val="une substance";
    else if($node[$n]['name'] == "_INFO-SEM-THING-ABSTR")
        $node_val="une chose abstraite";
    else if($node[$n]['name'] == "_INFO-LTECH")
        $node_val="vocabulaire technique";
    else if($node[$n]['name'] == "_INFO-BAD-ORTHO")
        $node_val="mauvaise orthographe";
    else if($node[$n]['name'] == "_INFO-SEM-CARAC")
        $node_val="une caractéristique";
    else if($node[$n]['name'] == "_INFO-SEM-THING-NATURAL")
        $node_val="un chose naturelle";
    else if($node[$n]['name'] == "_INFO-SEM-LIVING-BEING")
        $node_val="un etre vivant";
    else if($node[$n]['name'] == "_INFO-SEM-THING-ARTEFACT")
        $node_val="un artefact (chose construite par l’homme)";
    else if($node[$n]['name'] == "_INFO-SEM-ACTION")
        $node_val="une action";
    else if($node[$n]['name'] == "_INFO-SEM-TIME")
        $node_val="rapport avec le temps";
    else if($node[$n]['name'] == "_INFO-SEM-SET")
        $node_val="un ensemble";
    else if($node[$n]['name'] == "_INFO-VOC-COMMON")
        $node_val="vocabulaire courant";
    else if($node[$n]['name'] == "_INFO-SEM-IMAGINARY")
        $node_val="chose imaginaire";
    else if($node[$n]['name'] == "_INFO-MEANING-FIGURED")
        $node_val="sens figuré";
    else if($node[$n]['name'] == "_INFO-POLYSEMIC")
        $node_val="terme polysemique";
    else if($node[$n]['name'] == "_POL-NEG")
        $node_val="polarité negative";
    else if($node[$n]['name'] == "_POL-POS")
        $node_val="polarité positive";
    else if($node[$n]['name'] == "_INFO-LHYPOCORISTIC")
        $node_val="hypocoristiques";
    else if($node[$n]['name'] == "_POL-NEUTRE")
        $node_val="olarité neutre";
    else if($node[$n]['name'] == "_INFO-LRARE")
        $node_val="terme rare";
    else if($node[$n]['name'] == "_SEX_YES")
        $node_val="rapport avec le sexe";
    else if($node[$n]['name'] == "_SEX_NO")
        $node_val="sans rapport avec le sexe";
    else if($node[$n]['name'] == "_INFO-SEM-PERS-FEM")
        $node_val="une personne fem";
    else if($node[$n]['name'] == "_INFO-SEM-PERS-MASC")
        $node_val="une personne masc";
    else if($node[$n]['name'] == "_INFO-MEANING-LITERAL")
        $node_val="sens literal";
    else if($node[$n]['name'] == "_INFO-MONOSEMIC")
        $node_val="terme monosemique";
    else if($node[$n]['name'] == "_INFO-SEM-NAMED-ENTITY")
        $node_val="entité nommée";
    else if($node[$n]['name'] == "_INFO-SEM-COLOR-RELATED")
        $node_val="en rapport avec les couleurs/apparences";
    else if($node[$n]['name'] == "_INFO-SEM-EMOTION-RELATED")
        $node_val="en rapport avec les émotions";
    else if($node[$n]['name'] == "_INFO-MEANING-PREFERED")
        $node_val="sens préféré";
    else if($node[$n]['name'] == "_INFO-POLYMORPHIC")
        $node_val="termes ayant plusieurs morphologie";
    else if($node[$n]['name'] == "_INFO-SEM-PLACE-GEO")
        $node_val="lieu géographique";
    else if($node[$n]['name'] == "_INFO-SEM-PLACE-HUMAN")
        $node_val="lieu humain";
    else if($node[$n]['name'] == "_INFO-BIZARRE")
        $node_val="entrée bizarre";
    else if($node[$n]['name'] == "_INFO-VOC-HARD")
        $node_val="vocaulaire difficile";
    else if($node[$n]['name'] == "_INFO-SEM-THING")
        $node_val="chose";
    else if($node[$n]['name'] == "_INFO-SEM-PLACE-ANATOMICAL")
        $node_val="lieu anatomique";
    else if($node[$n]['name'] == "_INFO-SEM-PLACE-ABSTRACT")
        $node_val="lieu abstrait";
    else if($node[$n]['name'] == "_INFO-COUNTABLE-YES")
        $node_val="countable/énumérable";
    else if($node[$n]['name'] == "_INFO-COUNTABLE-NO")
        $node_val="non countable/énumérable";
    else if($node[$n]['name'] == "_INFO-SEM-PROPERTY-NAME")
        $node_val="nom de propriété";
    else if($node[$n]['name'] == "_INFO-SEM-QUANTIFIER")
        $node_val="quantifieur (comme brin dans brin d’herbe, ou pincée, dans pincée de sel)";
    else if($node[$n]['name'] == "_INFO-BAD-CASE")
        $node_val="mauvaise casse (maj, min)";
    else if($node[$n]['name'] == "_INFO-SEM-PHENOMENA")
        $node_val="phénomène";
    else if($node[$n]['name'] == "_INFO-SEM-STATE")
        $node_val="état";
    else
        $node_val=$node[$n]['name'];

    return $node_val;

}

function getPOS($node, &$n){
    $node_val="";
    if($node[$n]['name'] == "Nom:Fem+SG")
        $node_val="Féminin singulier";
    else if($node[$n]['name'] == "Ver:Inf")
        $node_val="Verbe infinitif";
    else if($node[$n]['name'] == "Adj:InvGen+SG")
        $node_val="Adjectif invariable (genre) singulier";
    else if($node[$n]['name'] == "Nom:InvGen+SG")
        $node_val="Nom invariable (genre) singulier";
    else if($node[$n]['name'] == "Nom:Mas+SG")
        $node_val="Nom masculin singulier";
    else if($node[$n]['name'] == "Nom:Mas+InvPL")
        $node_val="Nom masculin invariable pluriel";
    else if($node[$n]['name'] == "Nom:Mas+PL")
        $node_val="Nom masculin pluriel";
    else if($node[$n]['name'] == "Adj:Mas+SG")
        $node_val="Adjectif masculin singulier";
    else if($node[$n]['name'] == "Adv:")
        $node_val="Adverbe";
    else if($node[$n]['name'] == "Mas+SGDemPro2")
        $node_val="Masculin singulier";
    else if($node[$n]['name'] == "Adj:InvGen+SG:InvGen+PL")
        $node_val="Adjectif invariable (genre) singulier pluriel";
    else if($node[$n]['name'] == "Adj:Mas+InvPL")
        $node_val="Adjectif masculin invariable pluriel";
    else if($node[$n]['name'] == "Nom:Fem+PL")
        $node_val="Nom féminin pluriel";
    else if($node[$n]['name'] == "Adj:Fem+SG")
        $node_val="Adjectif féminin singulier";
    else if($node[$n]['name'] == "Nom:Fem+InvPL")
        $node_val="Nom féminin invariable pluriel";
    else if($node[$n]['name'] == "Con:")
        $node_val="Conjonction";
    else if($node[$n]['name'] == "Adj:InvGen+InvPL")
        $node_val="Adjectif invariable (genre) invariable (pluriel)";
    else if($node[$n]['name'] == "Int:")
        $node_val="Interjection";
    else if($node[$n]['name'] == "Abr:")
        $node_val="Abbréviation";
    else if($node[$n]['name'] == "Pro:SG+P3:PL+P3")
        $node_val="Pronom (sg+p3, pl+p3)";
    else if($node[$n]['name'] == "Det:Mas+SG")
        $node_val="Determinant masculin singulier";
    else if($node[$n]['name'] == "Pro:SG+P3PL+P3")
        $node_val="Pronom (sg+p3, pl+p3)";
    else if($node[$n]['name'] == "Pro:Mas+SG")
        $node_val="Pronom masculin singulier";
    else if($node[$n]['name'] == "Fem+SG+DemPro+Prox")
        $node_val="Féminim singulier démonstratif";
    else if($node[$n]['name'] == "Mas+SG+DemPro+Prox")
        $node_val="Masculin singulier démonstratif";
    else if($node[$n]['name'] == "Det:InvGen+PL")
        $node_val="Déterminant invariable (genre) pluriel";
    else if($node[$n]['name'] == "Det:Mas+PL")
        $node_val="Déterminant masculin pluriel";
    else if($node[$n]['name'] == "Pro:Mas+PL")
        $node_val="Pronom masculin pluriel";
    else if($node[$n]['name'] == "Adj:InvGen+PL")
        $node_val="Adjectif invariable (genre) pluriel";
    else if($node[$n]['name'] == "Det:InvGen+SG")
        $node_val="Déterminant invariable (genre) singulier";
    else if($node[$n]['name'] == "Pre:")
        $node_val="Préposition";
    else if($node[$n]['name'] == "Ver:Inf+:IPre+SG+P1:IPre+SG+P3:SPre+SG+P1:SPre+SG+P3:ImPre+SG+P2")
        $node_val="Verbe infinitif";
    else if($node[$n]['name'] == "Adj:Mas+PL")
        $node_val="Adjectif masculin pluriel";
    else if($node[$n]['name'] == "Nom:InvGen+SG:InvGen+PL")
        $node_val="Nom invariable";
    else if($node[$n]['name'] == "Adj:Fem+InvPL")
        $node_val="Adjectif féminin invariable pluriel";
    else if($node[$n]['name'] == "Det:")
        $node_val="Déterminant";
    else if($node[$n]['name'] == "Adj:Card")
        $node_val="Adjectif cardinal";
    else if($node[$n]['name'] == "Pro:")
        $node_val="Pronom";
    else if($node[$n]['name'] == "Pro:3Fem+SG")
        $node_val="Pronom (3ème personne du féminin singulier)";
    else if($node[$n]['name'] == "Pro:3Fem+PL")
        $node_val="Pronom (3ème personne du féminin pluriel)";
    else if($node[$n]['name'] == "Pro:3Mas+PL")
        $node_val="Pronom (3ème personne du masculin singulier)";
    else if($node[$n]['name'] == "Det:InvGen+SG:InvGen+PL")
        $node_val="Déterminant invariable";
    else if($node[$n]['name'] == "Nom:InvGen+InvPL")
        $node_val="Nom invariable";
    else if($node[$n]['name'] == "Nom:InvGen+PL")
        $node_val="Nom invariable";
    else if($node[$n]['name'] == "Pro:3Mas+SG")
        $node_val="Pronom masculin singulier";
    else if($node[$n]['name'] == "Pro:SG+P1")
        $node_val="Pronom singulier";
    else if($node[$n]['name'] == "Pro:PL+P3")
        $node_val="Pronom pluriel";
    else if($node[$n]['name'] == "Pro:InvGen+SG")
        $node_val="Pronom invariable singulier";
    else if($node[$n]['name'] == "Pro:SG+P3")
        $node_val="Pronom singulier";
    else if($node[$n]['name'] == "Nom+InvGen+SG+P3+PC")
        $node_val="Nom invariable singulier";
    else if($node[$n]['name'] == "Nom:SG+Card")
        $node_val="Nom singulier cardinal";
    else if($node[$n]['name'] == "Pro:PL+P1")
        $node_val="Pronom pluriel";
    else if($node[$n]['name'] == "Adj:Fem+PL")
        $node_val="Adjectif féminin pluriel";
    else if($node[$n]['name'] == "Pro:InvGen+PL")
        $node_val="Pronom invariable pluriel";
    else if($node[$n]['name'] == "Ver:IPre+SG+P1")
        $node_val="Verbe";
    else if($node[$n]['name'] == "Ver:SPre+SG+P1")
        $node_val="Verbe";
    else if($node[$n]['name'] == "Ukn:")
        $node_val="Unknown";
    else if($node[$n]['name'] == "Nom:")
        $node_val="Nom";
    else if($node[$n]['name'] == "Adj:")
        $node_val="Adjectif";
    else if($node[$n]['name'] == "Ver:")
        $node_val="Verbe";
    else if($node[$n]['name'] == "Suffix:")
        $node_val="Suffixe";
    else if($node[$n]['name'] == "Card:")
        $node_val="Cardinal";
    else if($node[$n]['name'] == "Concept:")
        $node_val="Concepte";
    else if($node[$n]['name'] == "Modifier:")
        $node_val="Modifier";
    else if($node[$n]['name'] == "Prefix:")
        $node_val="Prefixe";
    else if($node[$n]['name'] == "Info:")
        $node_val="Info";
    else if($node[$n]['name'] == "Expression:")
        $node_val="Expression";
    else if($node[$n]['name'] == "Relation:")
        $node_val="Relation";
    else if($node[$n]['name'] == "Symbole:")
        $node_val="Symbole";
    else if($node[$n]['name'] == "anatomie")
        $node_val="Anatomie";
    else if($node[$n]['name'] == "Conj:Coord")
        $node_val="Conjonction de coordination";
    else if($node[$n]['name'] == "Gender:Mas")
        $node_val="Genre masculin";
    else if($node[$n]['name'] == "Gender:Fem")
        $node_val="Genre feminin";
    else if($node[$n]['name'] == "Gender:Inv")
        $node_val="Genre invariable";
    else if($node[$n]['name'] == "Number:Sing")
        $node_val="Nombre singulier";
    else if($node[$n]['name'] == "Number:Plur")
        $node_val="Genre masculin";
    else if($node[$n]['name'] == "Number:Inv")
        $node_val="Numbre invariable";
    else if($node[$n]['name'] == "Link:")
        $node_val="Lien";
    else if($node[$n]['name'] == "Analogy:")
        $node_val="Analogie";
    else if($node[$n]['name'] == "Tournure:Impersonnelle")
        $node_val="Tournure impersonnelle";
    else if($node[$n]['name'] == "")
        $node_val="";
    else if($node[$n]['name'] == "")
        $node_val="";
    else if($node[$n]['name'] == "")
        $node_val="";
    else
        $node_val=$node[$n]['name'];

    return $node_val;
}
