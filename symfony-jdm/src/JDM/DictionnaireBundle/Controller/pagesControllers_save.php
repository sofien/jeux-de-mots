<?php

namespace JDM\DictionnaireBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends Controller
{
    /*public function indexAction()
    {
        return $this->render('JDMDictionnaireBundle::index.html.twig');
    }*/
    public function researchAction(Request $request)
    {
        $infos1=array();
        $infos1[]='Bloc 1 - Premiere ligne 1';
        $infos1[]='Bloc 1 - Premiere ligne 2';
        $infos1[]='Bloc 1 - Premiere ligne 3';
        $infos2=array();
        $infos2[]='Bloc 2 - Premiere ligne 1';
        $infos2[]='Bloc 2 - Premiere ligne 2';
        $infos2[]='Bloc 2 - Premiere ligne 3';
        $infos3=array();
        $infos3[]='Bloc 3 - Premiere ligne 1';
        $infos3[]='Bloc 3 - Premiere ligne 2';
        $infos3[]='Bloc 3 - Premiere ligne 3';
        $infos4=array();
        $infos4[]='Bloc 4 - Premiere ligne 1';
        $infos4[]='Bloc 4 - Premiere ligne 2';
        $infos4[]='Bloc 4 - Premiere ligne 3';


        // 1 : on ouvre le fichier
        $monfichier = fopen('lci.list', 'r+');

        // 2 : on lit la première ligne du fichier
        $ligne = fgets($monfichier);
        $ligne_1 = fgets($monfichier);
        $ligne_2 = fgets($monfichier);
        $ligne_3 = fgets($monfichier);
        $ligne_4 = fgets($monfichier);

        die($ligne_4);

        // 3 : quand on a fini de l'utiliser, on ferme le fichier
        fclose($monfichier);


        $data = array();
        $form = $this->createFormBuilder($data)
                     ->add('word', TextType::class, array('attr' => array('class' => 'input-recherche form-control wordset', 'list'=> 'wordsetlist', 'placeholder'=>'Entrez un mot', 'style'=>'height:25px;'), 'label'=>false))
                     ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* On recupere les données */
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $wordset = $em->getRepository('JDMDictionnaireBundle:Node')->findBy(array('name'=> $data['word']));

            /* On change de page pour aller a la page principale */
            return $this->redirectToRoute('jdm_dictionnaire_dictionarypage', array('word' => $data['word'], ));
        }

        return $this->render('JDMDictionnaireBundle:Pages:research.html.twig', array('form' => $form->createView(), 'infos1' => $infos1, 'infos2' => $infos2, 'infos3' => $infos3, 'infos4' => $infos4));
    }

    public function dictionaryAction($word, $sortType, $page, $numPage, Request $request)
    {

        if($page === -1){
            $definition = array();
            $definition[]="Definition 1 pour le fun de dezd ed zdede zd ez dezd dezde gtrgt r dedez defre fr egtgtr  ez dezd dezde gtrgt r dedez defre fr egtgtr  ez dezd dezde gtrgt r dedez defre fr egtgtr g";
            $definition[]= array(1=>'Definition 2 pour le fun 1',2 => 'Definition 2 pour le fun 2');
            $definition[]="Definition 3 pour le fun";
            $definition[]= array(1 => 'Definition 4 pour le fun 1', 2 => 'Definition 4 pour le fun 2');

            $query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT  DISTINCT r.t, rt.extendsName, n2.name, n2.formatedName, n1.eid, r.w
                                            FROM   JDMDictionnaireBundle:Relations r, JDMDictionnaireBundle:Relation_type rt, JDMDictionnaireBundle:Node n1, JDMDictionnaireBundle:Node n2
                                            WHERE  n1.name=:word
                                            AND r.t=rt.rtid
                                            AND r.eid1=n1.eid
                                            AND r.eid2=n2.eid
                                            GROUP BY r.t, rt.extendsName, n2.name, n2.formatedName, n1.eid, r.w'
                                        )
                            /*->createQuery( 'SELECT  r.t, rt.extendsName, n2.name, n2.formatedName
                                            FROM   JDMDictionnaireBundle:Relations r, JDMDictionnaireBundle:Relation_type rt, JDMDictionnaireBundle:Node n1, JDMDictionnaireBundle:Node n2
                                            WHERE  n1.name=:word
                                            AND r.t=rt.rtid
                                            AND r.t<>4
                                            AND r.t<>12
                                            AND r.t<>18
                                            AND r.t<>19
                                            AND r.t<>23
                                            AND r.t<>24
                                            AND r.t<>26
                                            AND r.t<>27
                                            AND r.t<>42
                                            AND r.t<>48
                                            AND r.t<>47
                                            AND r.t<>46
                                            AND r.t<>45
                                            AND r.t<>66
                                            AND r.t<>114
                                            AND r.t<>118
                                            AND r.t<>155
                                            AND r.t<>156
                                            AND r.t<>444
                                            AND r.t<>555
                                            AND r.t<>666
                                            AND r.t<>777
                                            AND r.t<>998
                                            AND r.t<>999
                                            AND r.t<>1000
                                            AND r.t<>1001
                                            AND r.t<>1002
                                            AND r.t<>2001
                                            AND r.eid1=n1.eid
                                            AND r.eid2=n2.eid
                                            GROUP BY r.t, rt.extendsName, n2.name, n2.formatedName'
                                        )*/
                            ->setParameter('word', $word);
            $node = $query->getResult();

            /*-----------------------------------------------------------*/
            /* Verification pour savoir si le mot existe ou nom */
            if (null === $node || count($node)===0) { //Verification pour savoir si le mot existe ou nom
              throw new NotFoundHttpException("Le mot ".$word." n'existe pas.");
            }


            /*-----------------------------------------------------------*/
            /* Requete pour la polarite */
            $idword=$node[0]["eid"];
            $query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT r.w, r.eid2
                                            FROM  JDMDictionnaireBundle:Relations r
                                            WHERE r.eid1=:idword
                                            AND (r.eid2=254878
                                            OR r.eid2=254877
                                            OR r.eid2=254876)'
                                        )
                            ->setParameter('idword', $idword);
            $polarite = $query->getResult();
            //die(var_dump($polarite_neg));

            /*-----------------------------------------------------------*/
            /* regroupement des type de relation dans un tableau */
            $relationType = array();
            for($n =0 ; $n< count($node); $n++){
                if(!in_array($node[$n]["extendsName"], $relationType))
                    $relationType[]=$node[$n]["extendsName"];
            }

            /*-----------------------------------------------------------*/
            /* regroupement des valeur par les types de relation */
            $nodeRes = array();
            for($n =0 ; $n< count($node); $n++){
                for($rt =0 ; $rt< count($relationType); $rt++){
                    if($node[$n]['extendsName']===$relationType[$rt]){
                        //if(isset($nodeRes[$relationType[$rt]])){
                        //    $nodeRes[$relationType[$rt]][]=$node[$n];
                        //    continue;
                        //}
                        //else{
                            $nodeRes[$relationType[$rt]][]=$node[$n];
                            continue;
                        //}
                    }
                }
            }
            //die(var_dump($nodeRes));
            //die(var_dump($node[0]["extendsName"]));
            //die(var_dump($relationType));


            /*-----------------------------------------------------------*/
            /* requetes de Definition */
            /*requete pour la definition*/
            /*$query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT d.def
                                            FROM  JDMDictionnaireBundle:Definition d
                                            WHERE d.termid=:idword
                                            limit 1'
                                        )
                            ->setParameter('idword', $idword);
            $definition = $query->getResult();*/
            //die(var_dump($definition));
        }
        /*-----------------------------------------------------------*/
        /* pagination */
        /*for ($i=0; $i < count($relationType); $i++) {
            $noderes_pagination[$relationType[$i]] = $this->get('knp_paginator')->paginate($nodeRes[$relationType[$i]], $request->query->get('page'.$i, 1), 50);
        }*/
        //$paginator  = $this->get('knp_paginator')->paginate($query, $request->query->get('page', 1), 10);
        $noderes_pagination=$nodeRes;

        /*-----------------------------------------------------------*/
        /* Formulaire de recherche */
        $data = array();
        $form = $this->createFormBuilder($data)
                     ->add('word', TextType::class, array('attr' => array('class' => 'input-recherche-barre form-control', 'placeholder'=>'Entrez un mot', 'label'=>false), 'label'=>false))
                     ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /* On recupere les données */
            $data = $form->getData();
            /* On change de page pour aller a la page principale */
            return $this->redirectToRoute('jdm_dictionnaire_dictionarypage', array('word' => $data['word']));
        }

        //die(var_dump($node));
        return $this->render('JDMDictionnaireBundle:Pages:dictionary.html.twig', array('form' => $form->createView(), 'word'=>$word, 'definition' => $definition, 'relationtype' => $relationType, 'node' => $noderes_pagination, 'polarite' => $polarite));
    }



    public function autocompletionAction($word, Request $request){
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->add('select', 'n.name')
            ->add('from', 'JDMDictionnaireBundle:Node n')
            ->add('where', $qb->expr()->like('n.name', '?1'))
            ->setMaxResults('7')
            ->setParameter(1, $word.'%');
        $query = $qb->getQuery();
        $ndset = $query->getResult();
        $nodeset=NULL;
        foreach ($ndset as $key => $value) {
            $nodeset[]=$value;
        }
        //$nodeset="soka";
        //die(var_dump($nodeset));

        $response = new JsonResponse();
        return $response->setData(array('nodeset'=>$nodeset));
    }
}
















<?php

namespace JDM\DictionnaireBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PagesController extends Controller
{
    /*public function indexAction()
    {
        return $this->render('JDMDictionnaireBundle::index.html.twig');
    }*/
    public function researchAction(Request $request)
    {
        $infos1=array();
        $infos1[]='Bloc 1 - Premiere ligne 1';
        $infos1[]='Bloc 1 - Premiere ligne 2';
        $infos1[]='Bloc 1 - Premiere ligne 3';
        $infos2=array();
        $infos2[]='Bloc 2 - Premiere ligne 1';
        $infos2[]='Bloc 2 - Premiere ligne 2';
        $infos2[]='Bloc 2 - Premiere ligne 3';
        $infos3=array();
        $infos3[]='Bloc 3 - Premiere ligne 1';
        $infos3[]='Bloc 3 - Premiere ligne 2';
        $infos3[]='Bloc 3 - Premiere ligne 3';
        $infos4=array();
        $infos4[]='Bloc 4 - Premiere ligne 1';
        $infos4[]='Bloc 4 - Premiere ligne 2';
        $infos4[]='Bloc 4 - Premiere ligne 3';

        $data = array();
        $form = $this->createFormBuilder($data)
                     ->add('word', TextType::class, array('attr' => array('class' => 'input-recherche form-control wordset', 'list'=> 'wordsetlist', 'placeholder'=>'Entrez un mot', 'style'=>'height:25px;'), 'label'=>false))
                     ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* On recupere les données */
            $data = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $wordset = $em->getRepository('JDMDictionnaireBundle:Node')->findBy(array('name'=> $data['word']));

            /* On change de page pour aller a la page principale */
            return $this->redirectToRoute('jdm_dictionnaire_dictionarypage', array('word' => $data['word'], ));
        }

        return $this->render('JDMDictionnaireBundle:Pages:research.html.twig', array('form' => $form->createView(), 'infos1' => $infos1, 'infos2' => $infos2, 'infos3' => $infos3, 'infos4' => $infos4));
    }

    public function dictionaryAction($word, $sortType, $page, $numPage, Request $request)
    {

        if($page === -1){
            $definition = array();
            $definition[]="Definition 1 pour le fun de dezd ed zdede zd ez dezd dezde gtrgt r dedez defre fr egtgtr  ez dezd dezde gtrgt r dedez defre fr egtgtr  ez dezd dezde gtrgt r dedez defre fr egtgtr g";
            $definition[]= array(1=>'Definition 2 pour le fun 1',2 => 'Definition 2 pour le fun 2');
            $definition[]="Definition 3 pour le fun";
            $definition[]= array(1 => 'Definition 4 pour le fun 1', 2 => 'Definition 4 pour le fun 2');


            //Requete pour avoir tous les relations types
            $queryRelationType = $this->getDoctrine()
                                      ->getManager()
                                      ->createQuery( 'SELECT rt.rtid, rt.extendsName FROM   JDMDictionnaireBundle:Relation_type rt' );
            $relationTypes = $queryRelationType->getResult();

            //die(var_dump($relationTypes));

            //Tous les noeuds regroupé par type
            $nodeRes = array();
            $relationType = array();
            for ($i=0; $i < /*count($relationTypes)*/50; $i++) {
                //Requete pour avoir tous les relations pour chaque $relationTypes[$i]
                $queryRelations = $this->getDoctrine()
                                       ->getManager()
                                       ->createQuery( 'SELECT  DISTINCT r.t, rt.extendsName, n2.name, n2.formatedName, n1.eid, r.w, r.eid2, rt.info
                                                       FROM   JDMDictionnaireBundle:Relations r, JDMDictionnaireBundle:Relation_type rt, JDMDictionnaireBundle:Node n1, JDMDictionnaireBundle:Node n2
                                                       WHERE  n1.name=:word
                                                       AND r.t=rt.rtid
                                                       AND r.t=:type
                                                       AND r.eid1=n1.eid
                                                       AND r.eid2=n2.eid
                                                       '
                                                       //GROUP BY r.t, rt.extendsName, n2.name, n2.formatedName, n1.eid, r.w, r.eid2, rt.info
                                                   )
                                        ->setParameter('word', $word)
                                        ->setParameter('type', $relationTypes[$i]['rtid'])
                                        ->setFirstResult(0)
                                        ->setMaxResults(10);
                $relations = $queryRelations->getResult();

                $nodeRes[$relationTypes[$i]['extendsName']] = $relations;
                //die(var_dump($nodeRes));

                //Ajout dans le tableau $relationType les noms des types de relation.
                if(!in_array($relationTypes[$i]["extendsName"], $relationType))
                    $relationType[]=$relationTypes[$i]["extendsName"];
            }
            /*-----------------------------------------------------------*/
            /* Verification pour savoir si le mot existe ou nom */
            if (null === $nodeRes || count($nodeRes)===0) { //Verification pour savoir si le mot existe ou nom
                throw new NotFoundHttpException("Le mot ".$word." n'existe pas.");
            }
            /*=================================*/
            /*=========== A CHANGER ===========*/
            /*=================================*/
            /*-----------------------------------------------------------*/
            /* Requete pour la polarite */
            $query = $this  ->getDoctrine()
                            ->getManager()
                            ->createQuery( 'SELECT r.w, r.eid2
                                            FROM  JDMDictionnaireBundle:Relations r
                                            WHERE r.eid1=:idword
                                            AND (r.eid2=254878
                                            OR r.eid2=254877
                                            OR r.eid2=254876)'
                                        )
                            ->setParameter('idword', $word);
            $polarite = $query->getResult();
        }
        /*-----------------------------------------------------------*/
        /* pagination */
        /*for ($i=0; $i < count($relationType); $i++) {
            $noderes_pagination[$relationType[$i]] = $this->get('knp_paginator')->paginate($nodeRes[$relationType[$i]], $request->query->get('page'.$i, 1), 50);
        }*/
        //$paginator  = $this->get('knp_paginator')->paginate($query, $request->query->get('page', 1), 10);
        //$noderes_pagination=$nodeRes;

        /*-----------------------------------------------------------*/
        /* Formulaire de recherche */
        $data = array();
        $form = $this->createFormBuilder($data)
                     ->add('word', TextType::class, array('attr' => array('class' => 'input-recherche-barre form-control', 'placeholder'=>'Entrez un mot', 'label'=>false), 'label'=>false))
                     ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /* On recupere les données */
            $data = $form->getData();
            /* On change de page pour aller a la page principale */
            return $this->redirectToRoute('jdm_dictionnaire_dictionarypage', array('word' => $data['word']));
        }

        //die(var_dump($node));
        return $this->render('JDMDictionnaireBundle:Pages:dictionary.html.twig', array('form' => $form->createView(), 'word'=>$word, 'definition' => $definition, 'relationtype' => $relationType, 'node' => $nodeRes, 'polarite' => $polarite));
    }



    public function autocompletionAction($word, Request $request){
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb ->add('select', 'n.name')
            ->add('from', 'JDMDictionnaireBundle:Node n')
            ->add('where', $qb->expr()->like('n.name', '?1'))
            ->setMaxResults('7')
            ->setParameter(1, $word.'%');
        $query = $qb->getQuery();
        $ndset = $query->getResult();
        $nodeset=NULL;
        foreach ($ndset as $key => $value) {
            $nodeset[]=$value;
        }
        //$nodeset="soka";
        //die(var_dump($nodeset));

        $response = new JsonResponse();
        return $response->setData(array('nodeset'=>$nodeset));
    }
}
