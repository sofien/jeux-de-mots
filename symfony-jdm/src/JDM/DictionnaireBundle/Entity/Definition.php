<?php

namespace JDM\DictionnaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Definition
 *
 * @ORM\Table(name="definition")
 * @ORM\Entity(repositoryClass="JDM\DictionnaireBundle\Repository\DefinitionRepository")
 */
class Definition
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /*/ **
     * @var int
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\GeneratedValue(strategy="AUTO")
     * /
    private $idDef;*/

    /**
     * @var int
     *
     * @ORM\Column(name="termid", type="integer")
     */
    private $termid;

    /**
     * @var string
     *
     * @ORM\Column(name="def", type="text")
     */
    private $def;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="playerid", type="integer")
     */
    private $playerid;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set termid
     *
     * @param integer $termid
     *
     * @return Definition
     */
    public function setTermid($termid)
    {
        $this->termid = $termid;

        return $this;
    }

    /**
     * Get termid
     *
     * @return int
     */
    public function getTermid()
    {
        return $this->termid;
    }

    /**
     * Set def
     *
     * @param string $def
     *
     * @return Definition
     */
    public function setDef($def)
    {
        $this->def = $def;

        return $this;
    }

    /**
     * Get def
     *
     * @return string
     */
    public function getDef()
    {
        return $this->def;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Definition
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set playerid
     *
     * @param integer $playerid
     *
     * @return Definition
     */
    public function setPlayerid($playerid)
    {
        $this->playerid = $playerid;

        return $this;
    }

    /**
     * Get playerid
     *
     * @return int
     */
    public function getPlayerid()
    {
        return $this->playerid;
    }
}
