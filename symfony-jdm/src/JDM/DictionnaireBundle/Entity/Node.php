<?php

namespace JDM\DictionnaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Node
 *
 * @ORM\Table(name="node")
 * @ORM\Entity(repositoryClass="JDM\DictionnaireBundle\Repository\NodeRepository")
 */
class Node
{

    /**
     * @var int
     *
     * @ORM\Column(name="eid", type="integer")
     * @ORM\Id
     */
    private $eid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="t", type="integer")
     */
    private $t;

    /**
     * @var int
     *
     * @ORM\Column(name="w", type="integer")
     */
    private $w;

    /**
     * @var string
     *
     * @ORM\Column(name="formated_name", type="text", nullable=true)
     */
    private $formatedName;

    /**
     * Set eid
     *
     * @param integer $eid
     *
     * @return Node
     */
    public function setEid($eid)
    {
        $this->eid = $eid;

        return $this;
    }

    /**
     * Get eid
     *
     * @return int
     */
    public function getEid()
    {
        return $this->eid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Node
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set t
     *
     * @param integer $t
     *
     * @return Node
     */
    public function setT($t)
    {
        $this->t = $t;

        return $this;
    }

    /**
     * Get t
     *
     * @return int
     */
    public function getT()
    {
        return $this->t;
    }

    /**
     * Set w
     *
     * @param integer $w
     *
     * @return Node
     */
    public function setW($w)
    {
        $this->w = $w;

        return $this;
    }

    /**
     * Get w
     *
     * @return int
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * Set formatedName
     *
     * @param string $formatedName
     *
     * @return Node
     */
    public function setFormatedName($formatedName)
    {
        $this->formatedName = $formatedName;

        return $this;
    }

    /**
     * Get formatedName
     *
     * @return string
     */
    public function getFormatedName()
    {
        return $this->formatedName;
    }
}
