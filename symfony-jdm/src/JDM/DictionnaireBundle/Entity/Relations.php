<?php

namespace JDM\DictionnaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relations
 *
 * @ORM\Table(name="relations")
 * @ORM\Entity(repositoryClass="JDM\DictionnaireBundle\Repository\RelationsRepository")
 */
class Relations
{

    /**
     * @var int
     *
     * @ORM\Column(name="bdid", type="integer")
     * @ORM\Id
     */
    private $bdid;

    /**
     * @var int
     *
     * @ORM\Column(name="eid1", type="integer")
     */
    private $eid1;

    /**
     * @var int
     *
     * @ORM\Column(name="eid2", type="integer")
     */
    private $eid2;

    /**
     * @var int
     *
     * @ORM\Column(name="t", type="integer")
     */
    private $t;

    /**
     * @var int
     *
     * @ORM\Column(name="w", type="integer")
     */
    private $w;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bdid
     *
     * @param integer $bdid
     *
     * @return Relations
     */
    public function setBdid($bdid)
    {
        $this->bdid = $bdid;

        return $this;
    }

    /**
     * Get bdid
     *
     * @return int
     */
    public function getBdid()
    {
        return $this->bdid;
    }

    /**
     * Set eid1
     *
     * @param integer $eid1
     *
     * @return Relations
     */
    public function setEid1($eid1)
    {
        $this->eid1 = $eid1;

        return $this;
    }

    /**
     * Get eid1
     *
     * @return int
     */
    public function getEid1()
    {
        return $this->eid1;
    }

    /**
     * Set eid2
     *
     * @param integer $eid2
     *
     * @return Relations
     */
    public function setEid2($eid2)
    {
        $this->eid2 = $eid2;

        return $this;
    }

    /**
     * Get eid2
     *
     * @return int
     */
    public function getEid2()
    {
        return $this->eid2;
    }

    /**
     * Set t
     *
     * @param integer $t
     *
     * @return Relations
     */
    public function setT($t)
    {
        $this->t = $t;

        return $this;
    }

    /**
     * Get t
     *
     * @return int
     */
    public function getT()
    {
        return $this->t;
    }

    /**
     * Set w
     *
     * @param integer $w
     *
     * @return Relations
     */
    public function setW($w)
    {
        $this->w = $w;

        return $this;
    }

    /**
     * Get w
     *
     * @return int
     */
    public function getW()
    {
        return $this->w;
    }
}
