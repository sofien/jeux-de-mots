<?php

namespace JDM\DictionnaireBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Relation_type
 *
 * @ORM\Table(name="relation_type")
 * @ORM\Entity(repositoryClass="JDM\DictionnaireBundle\Repository\Relation_typeRepository")
 */
class Relation_type
{

    /**
     * @var int
     *
     * @ORM\Column(name="rtid", type="integer")
     * @ORM\Id
     */
    private $rtid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="extends_name", type="text", nullable=true)
     */
    private $extendsName;

    /**
     * @var string
     *
     * @ORM\Column(name="info", type="text", nullable=true)
     */
    private $info;


    /**
     * Set rtid
     *
     * @param integer $rtid
     *
     * @return Relation_type
     */
    public function setRtid($rtid)
    {
        $this->rtid = $rtid;

        return $this;
    }

    /**
     * Get rtid
     *
     * @return int
     */
    public function getRtid()
    {
        return $this->rtid;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Relation_type
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set extendsName
     *
     * @param string $extendsName
     *
     * @return Relation_type
     */
    public function setExtendsName($extendsName)
    {
        $this->extendsName = $extendsName;

        return $this;
    }

    /**
     * Get extendsName
     *
     * @return string
     */
    public function getExtendsName()
    {
        return $this->extendsName;
    }

    /**
     * Set info
     *
     * @param string $info
     *
     * @return Relation_type
     */
    public function setInfo($info)
    {
        $this->info = $info;

        return $this;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }
}
